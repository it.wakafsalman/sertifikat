<?php

namespace App\Http\Controllers;

use App\Models\Donatur;
use App\Imports\DonaturImport;
use Illuminate\Http\Request;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;
use DateTimeZone;
use DataTables;


class DonaturController extends Controller
{

    public function index(Request $request){

        if ($request->ajax()) {
            if(!empty($request->from_date)){
                $data = Donatur::select('*')
                            ->selectRaw('DATEDIFF(tanggal_buat, tanggal_indo) as akumulasi_waktu')
                            ->whereBetween('tanggal_buat', [$request->from_date, $request->to_date])
                            ->orderBy('id','DESC');
            }else{
                $data = Donatur::select('*')
                            ->selectRaw('DATEDIFF(tanggal_buat, tanggal_indo) as akumulasi_waktu')
                            ->orderBy('id','DESC');
            }
            return Datatables::of($data)->setRowId(function ($row) {
                                            return 'tr_'.$row->id;
                                        })
                                        ->addIndexColumn()
                                        ->addColumn('checkbox', function($row){
                                            return '<input type="checkbox" class="checkbox" data-id="'.$row->id.'">';
                                        })
                                        ->addColumn('no_sertifikat', function($row){
                                            return '<a href="/sertifikat/'.$row->id.'/preview">'.$row->no_sertifikat.'</a>';
                                        })
                                        ->addColumn('tanggal_buat', function($row){
                                            if($row->tanggal_buat != ""){
                                                return $row->tanggal_buat;
                                            }else{
                                                return '';
                                            }
                                        })
                                        ->addColumn('akumulasi_waktu', function($row){
                                            if($row->tanggal_buat != ""){
                                                return $row->akumulasi_waktu.' Hari';
                                            }else{
                                                return '';
                                            }
                                        })
                                        ->addColumn('nominal', function($row){
                                            return 'Rp. '.number_format($row->nominal, 0, ',', '.').'.00,-';
                                        })
                                        ->addColumn('action', function($row){
                                            $btn = '<div class="btn-group">
                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Aksi
                                                        <span class="fa fa-caret-down"></span></button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="/eksport_pdf/'.$row->id.'">Eksport PDF</a></li>
                                                            <li><a href="/print_pdf/'.$row->id.'">Print PDF</a></li>
                                                            <li><a href="javascript:void(0)" onClick="editFunc('.$row->id.')" >Edit</a></li>
                                                            <li><a href="javascript:void(0);" id="hapus_donatur" onClick="hapusFunc('.$row->id.')">
                                                            Delete
                                                        </a></li>
                                                        </ul>
                                                    </div>';
                                            return $btn;
                                        })
                                        ->rawColumns(['checkbox','no_sertifikat','tanggal_buat','akumulasi_waktu','nominal','action'])
                                        ->make(true);
        }
        $judul      =   'Donasi';
        $statistik_donatur = Donatur::select('tanggal')
                                    ->selectRaw('count(nama) as jumlah_donatur')
                                    ->groupby('tanggal')
                                    ->orderby('tanggal')
                                    ->get();
        $statistik_donasi  = Donatur::select('tanggal')
                                    ->selectRaw('sum(nominal) as jumlah_donasi')
                                    ->groupby('tanggal')
                                    ->orderby('tanggal')
                                    ->get();
        //return view('donatur/data', compact('data','judul','statistik_donatur','statistik_donasi'));
        return view('donatur/data', compact('judul','statistik_donatur','statistik_donasi'));

    }


    public function tambah_donatur(Request $request){

        $timezone       =   'Asia/Jakarta';
        $date           =   new DateTime('now', new DateTimeZone($timezone));
        $tanggal_buat   =   $date->format('Y-m-d H:i:s');

        $tanggal = explode('-', $request->tanggal);
        $tahun = $tanggal[0];
        $bulan = $tanggal[1];

        $ambil_data = Donatur::whereYear('tanggal', $tahun)
                        ->whereMonth('tanggal', $bulan)
                        ->get();

        $cek = count($ambil_data);

        if($cek == 0){
            $angka = sprintf("%03s", 1);
            $no_sertifikat = $angka."/WSI/".$bulan."/".$tahun;          
        }else{
            $angka = sprintf("%03s", $cek + 1);
            $no_sertifikat = $angka."/WSI/".$bulan."/".$tahun;          

        }

        $data = Donatur::updateOrCreate(
            [
                'id'                =>  $request->id
            ],
            [
                'no_sertifikat'     =>  $no_sertifikat,
                'tanggal_indo'      =>  $request->tanggal,
                'tanggal'           =>  $request->tanggal,
                'tanggal_buat'      =>  $tanggal_buat,
                'nama'              =>  $request->nama,
                'email'             =>  $request->email,
                'no_telepon'        =>  $request->no_telepon,
                'alamat'            =>  $request->alamat,
                'nominal'           =>  $request->nominal,
                'tipe_donasi'       =>  $request->tipe_donasi,
                'program_donasi'    =>  $request->program_donasi,
                'tampil_alamat'     =>  $request->tampil_alamat,
                'no_resi'           =>  $request->no_resi,  
                'crm'               =>  $request->crm,
            ]
        );
        return Response()->json($data);

    }

    public function rubah_donatur(Request $request){

        $where = array('id' => $request->id);
        $donatur  = Donatur::where($where)->first();
      
        return Response()->json($donatur);

    }

    public function hapus_donatur(Request $request){

        $donatur = Donatur::where('id',$request->id)->delete();      
        return Response()->json($donatur);

    }

    public function hapus_donatur_pilihan(Request $request){

        $ids = $request->ids;
        Donatur::whereIn('id',explode(",", $ids))->delete();
        return response()->json(['status' => true, 'message' => 'Data berhasil dihapus']);

    }

    public function eksport_pdf($id){

        $data = Donatur::find($id);

        $no_sertifikat = explode('/', $data['no_sertifikat']);

        $nomor = $no_sertifikat[0];
        $bulan = $no_sertifikat[2];
        $tahun = $no_sertifikat[3];
        $pdf = PDF::loadview('donatur/data_pdf', compact('data'))->setPaper('a4', 'landscape');
        return $pdf->download('Sertifikat-'.$nomor.'-WSI-'.$bulan.'-'.$tahun.'.pdf');

    }

    public function print_pdf($id){

        $data = Donatur::find($id);

        $pdf = PDF::loadview('donatur/data_pdf', compact('data'))->setPaper('a4', 'landscape');
        return $pdf->stream();

    }

    public function import_excel(Request $request) {

        $data = $request->file('file');

        $nama_file = $data->getClientOriginalName();
        $data->move('DataDonatur', $nama_file);

        Excel::import(new DonaturImport, \public_path('/DataDonatur/'.$nama_file));
        return \redirect()->back();
        
    }

}
