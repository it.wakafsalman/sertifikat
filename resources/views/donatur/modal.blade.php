<!-- Import Data -->
<div class="modal fade" id="import-donatur">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Import Data Donatur</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form action="/import_excel" method="POST" enctype="multipart/form-data">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputName" class="form-label">Pilih file <a href="{{ asset('import_template/template_import.xlsx')  }}">* Download Template</a></label>
                        <input type="file" name="file" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Kembali</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Tambah Data -->
<div class="modal fade" id="crud-donatur">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="crud-donatur-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            </div>
            <form role="form" action="javascript:void(0)" method="POST" id="form-donatur" name="form-donatur">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputName" class="form-label">Nama Donatur <span class="text-danger">*</span></label>
                        <textarea type="text" name="nama" class="form-control" id="nama" style="height:80px;" required></textarea>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label for="exampleInputEmail" class="form-label">Email</label>
                                <input type="text" name="email" class="form-control" id="email">
                            </div>
                            <div class="col-xs-6">
                                <label for="exampleInputType" class="form-label">Nama CRM <span class="text-danger">*</span></label>
                                <select name="crm" id="crm" class="form-control" aria-label="Default select example" required>
                                    <option value="">Pilih CRM</option>
                                    <option value="Aji Abriansyah">Aji Abriansyah</option>
                                    <option value="Ai Supartini">Ai Supartini</option>
                                    <option value="Asri Ainun Syarifah">Asri Ainun Syarifah</option>
                                    <option value="Desty Fitria">Desty Fitria</option>
                                    <option value="Dini Lestari">Dini Lestari</option>
                                    <option value="Fitrindha Nurwulan">Fitrindha Nurwulan</option>
                                    <option value="Mega Sawiyyan">Mega Sawiyyan</option>
                                    <option value="Rini Astriani">Rini Astriani</option>
                                    <option value="Shofi Zakiyah">Shofi Zakiyah</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputAddress" class="form-label">Alamat</label>
                        <input type="textarea" name="alamat" class="form-control" id="alamat">
                        <input type="hidden" name="tampil_alamat" value="0">
                        <input type="checkbox" name="tampil_alamat" value="1"> Tampilkan Alamat
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <label for="exampleInputDate" class="form-label">Tanggal Donasi <span class="text-danger">*</span></label>
                                <input type="date" name="tanggal" class="form-control" id="tanggal" required>
                                <span class="text-danger error-text tanggal_error"></span>
                            </div>
                            <div class="col-xs-6">
                                <label for="exampleInputPrice" class="form-label">Nominal <span class="text-danger">*</span></label>
                                <input type="text" name="nominal" class="form-control" id="nominal" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputType" class="form-label">Donasi Tipe <span class="text-danger">*</span></label>
                        <select name="tipe_donasi" id="tipe_donasi" class="form-control" aria-label="Default select example" required>
                            <option value="">Pilih Donasi</option>
                            <option value="Wakaf & Infak">Wakaf & Infak</option>
                            <option value="Wakaf">Wakaf</option>
                            <option value="Donasi">Donasi</option>
                            <option value="Infak">Infak</option>
                            <option value="Sedekah">Sedekah</option>
                            <option value="CSR">CSR</option>
                            <option value="Hibah">Hibah</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputProgram" class="form-label">Program Donasi <span class="text-danger">*</span></label>
                        <input type="text" name="program_donasi" class="form-control" id="program_donasi" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Kembali</button>
                    <button type="submit" class="btn btn-primary" id="btn-save">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
