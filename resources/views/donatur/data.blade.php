@extends('layout.master')

@section('title', 'E-Sertifikasi Wakaf Salman ITB')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{$judul}}
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> {{$judul}}</a></li>
    </ol>
  </section>

  <section class="content-header">  
    <div class="row">
      <div class="col-xs-12">
        <div class="box-body pull-left">
          <div class="row input-daterange">
            <div class="col-md-3" style="margin-top: 6px;">
              Tanggal Buat Sertifikat :
            </div>
            <div class="col-md-3">
                <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" style="background-color: #ffffff;" readonly />
            </div>
            <div class="col-md-3">
                <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" style="background-color: #ffffff;" readonly />
            </div>
            <div class="col-md-3">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
            </div>
          </div>
        </div>
        <div class="box-body pull-right">
          <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Aksi
              <span class="fa fa-caret-down"></span></button>
            <ul class="dropdown-menu" role="menu">
              <li><a href="" class="hapus_semua">Hapus</a></li>
            </ul>
          </div>
          <a href="javascript:void(0)" class="btn btn-info mb-4" onClick="add()"><i class="glyphicon glyphicon-plus"></i> Tambah Sertifikat</a>
          <a href="javascript:void(0)" class="btn btn-success mb-4" onClick="upload()"><i class="glyphicon glyphicon-open"></i> Import Data</a>
        </div>
      </div>
    </div>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <!-- /.box -->

    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Donasi</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="donatur-datatable" class="table table-bordered table-striped donatur-datatable">
          <thead>
          <tr>
            <th>
              <strong><input type="checkbox" id="ceklis_semua"></strong>
            </th>
            <th>No</th>
            <th>No Sertifikat</th>
            <th>Tanggal Berwakaf</th>
            <th>Tanggal Buat Sertifikat</th>
            <th>Akumulasi Waktu</th>
            <th>Nama</th>
            <th>Nominal</th>
            <th>Tim CRM</th>
            <th>Aksi</th>
          </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>

  </section>
  <!-- /.content -->
</div>

@include('donatur.modal')  

@push('donatur')

<script type="text/javascript">

  $('.input-daterange').datepicker({
    todayBtn:'linked',
    format:'yyyy-mm-dd',
    autoclose:true
  });

  load_data();

  $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  function load_data(from_date = '', to_date = ''){
    $('#donatur-datatable').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        url:'{{ route("donatur") }}',
        data:{from_date:from_date, to_date:to_date}
      },
      columns: [
        {data: 'checkbox', name: 'checkbox', orderable: false},
            {data: 'id', name: 'id'},
            {data: 'no_sertifikat', name: 'no_sertifikat'},
            {data: 'tanggal_indo', name: 'tanggal_indo'},
            {data: 'tanggal_buat', name: 'tanggal_buat'},
            {data: 'akumulasi_waktu', name: 'akumulasi_waktu'},
            {data: 'nama', name: 'nama'},
            {data: 'nominal', name: 'nominal'},
            {data: 'crm', name: 'crm'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
      ]
    });
  }

  $('#filter').click(function(){
      var from_date = $('#from_date').val();
      var to_date = $('#to_date').val();
      if(from_date != '' &&  to_date != ''){
          $('#donatur-datatable').DataTable().destroy();
          load_data(from_date, to_date);
      }else{
          alert('Both Date is required');
      }
  });

  $('#refresh').click(function(){
      $('#from_date').val('');
      $('#to_date').val('');
      $('#donatur-datatable').DataTable().destroy();
      load_data();
  });

  function add(){
 
    $('#form-donatur').trigger("reset");
    $('#crud-donatur-title').html("Tambah Donatur");
    $('#crud-donatur').modal('show');
    $('#id').val('');

  }

  function upload(){
  
    $('#import-donatur').modal('show');
  
  }

  function editFunc(id){
     
     $.ajax({
         type:"POST",
         url: "{{ url('rubah_donatur') }}",
         data: { id: id },
         dataType: 'json',
         success: function(res){
           $('#crud-donatur-title').html("Rubah Donatur");
           $('#crud-donatur').modal('show');
           $('#id').val(res.id);
           $('#nama').val(res.nama);
           $('#email').val(res.email);
           $('#crm').val(res.crm);
           $('#alamat').val(res.alamat);
           $('#tanggal').val(res.tanggal);
           $('#nominal').val(res.nominal);
           $('#tipe_donasi').val(res.tipe_donasi);
           $('#program_donasi').val(res.program_donasi);
        }
     });
   }

  function hapusFunc(id){
    if (confirm("Apakah kamu yakin akan menghapus data ini?") == true) {
    var id = id;
      
      // ajax
      $.ajax({
          type:"POST",
          url: "{{ url('hapus_donatur') }}",
          data: { id: id },
          dataType: 'json',
          success: function(res){

            var oTable = $('#donatur-datatable').dataTable();
            oTable.fnDraw(false);
          }
      });
    }
  }

  $('#form-donatur').submit(function(e) {
 
    e.preventDefault();

    var formData = new FormData(this);

    $.ajax({
      type:'POST',
      url: "{{ url('tambah_donatur')}}",
      data: formData,
      cache:false,
      contentType: false,
      processData: false,
      success: (data) => {
        $("#crud-donatur").modal('hide');
        var oTable = $('#donatur-datatable').dataTable();
        oTable.fnDraw(false);
        $("#btn-save").html('Simpan');
        $("#btn-save"). attr("disabled", false);
      },
      error: function(data){
        console.log(data);
      }
    });
  });

  $(".filter").click(function(){
    var oTable = $('#donatur-datatable').dataTable();
    oTable.fnDraw(false);
  });


</script>
<script>
  $(document).ready(function(){
    $('#ceklis_semua').on('click', function(){
      if($(this).is(":checked", true)){
        $('.checkbox').prop('checked', true);
      }else{
        $('.checkbox').prop('checked', false);
      }
    });

    $('.checkbox').on('click', function(){
      if($('.checkbox:checked').length == $('.checkbox').length){
        $('#ceklis_semua').prop('checked', true);
      }else{
        $('#ceklis_semua').prop('checked', false);
      }
    });

    $('.hapus_semua').on('click', function(e) {
      var id_donatur_array = [];
      $('.checkbox:checked').each(function(){
        id_donatur_array.push($(this).attr('data-id'));
      });

      if(id_donatur_array.length <= 0){
        alert('Anda harus memilih paling tidak satu data donatur yang ingin dihapus');
      }else{
        if(confirm('Anda yakin ingin menghapus data donatur yang telah dipilih ini?')){
          var id_donatur_string = id_donatur_array.join(",");
          console.log(id_donatur_string);
          $.ajax({
            url: "/hapus_donatur_pilihan",
            type: 'DELETE',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: 'ids='+id_donatur_string,
            success:function(data){
              if(data['status' == true]){
                $('.checkbox:checked').each(function (){
                  $(this).parents("tr").remove();
                });
              }else{
                alert('Data berhasil dihapus');                }
            },
            error:function(data){
              alert(data.responseText);
            }
          });
        }
      }
    });
  });
</script>

@endpush

@endsection